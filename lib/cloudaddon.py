import os
import requests
import threading

import xbmcaddon
import xbmc


# noinspection PyPep8Naming,PyShadowingBuiltins
def Addon(id=None, force_new=False):
    addon_id = id

    instance = CloudAddon.instances.get(id, None)
    if not force_new and instance is not None:
        xbmc.log("[CloudSync] Re-using existing cloud-sync Addon instance")
        return instance

    if addon_id:
        instance = CloudAddon(addon_id)
    else:
        instance = CloudAddon()

    CloudAddon.instances[addon_id] = instance
    return instance


# noinspection PyShadowingBuiltins
class CloudAddon(xbmcaddon.Addon):
    instances = {}

    def __init__(self, id=None):
        if id:
            xbmcaddon.Addon.__init__(self, id)
        else:
            xbmcaddon.Addon.__init__(self)
            id = xbmcaddon.Addon.getAddonInfo(self, "id")

        self.settings = (xbmcaddon.Addon.getSetting(self, "cloud_sync_settings") or "").split(",")
        self.settings = filter(lambda s: s is not '', self.settings)
        xbmc.log("[CloudSync] Starting cloud-sync enabled Addon instance for %s with settings: %s" %
                 (id, ", ".join(self.settings) or "<none>", ), xbmc.LOGINFO)

        self.__addon_id = id
        self.__addon_name = xbmcaddon.Addon.getAddonInfo(self, "name")
        self.__addon_settings_dir = xbmc.translatePath("special://profile/addon_data/").decode('utf-8')
        self.__addon_settings_path = os.path.join(self.__addon_settings_dir, id, "settings.xml")
        self.__modified_time = -1

        self.__host = "localhost"
        self.__port = 5454
        self.__lock = threading.RLock()

        # Now notify the cloud sync that you are there
        self.__cloud_sync()
        return

    def getSetting(self, id):
        # self.__cloud_sync()
        return xbmcaddon.Addon.getSetting(self, id)

    def setSetting(self, id, value):
        xbmcaddon.Addon.setSetting(self, id, value)
        if id not in self.settings:
            return

        self.__cloud_sync()

    def __is_sync_enabled(self):
        cloud_sync_enabled = self.getSetting("cloud_sync_enabled") == "true"
        if not cloud_sync_enabled:
            xbmc.log("[CloudSync] Cloud-sync was disabled for this add-on using the 'cloud_sync_enabled' setting.",
                     xbmc.LOGWARNING)

        return cloud_sync_enabled

    def __cloud_sync(self, modified_time=0):
        if not self.__is_sync_enabled():
            return

        # the modified time
        if modified_time == 0 and os.path.exists(self.__addon_settings_path):
            modified_time = int(os.path.getmtime(self.__addon_settings_path))
        if modified_time == self.__modified_time:
            xbmc.log("[CloudSync] settings.xml not modified.", xbmc.LOGDEBUG)
            return

        # What add-on version?
        addon_version = xbmcaddon.Addon.getAddonInfo(self, "version")

        settings = {}
        for setting_id in self.settings:
            settings[setting_id] = xbmcaddon.Addon.getSetting(self, setting_id)

        new_settings = {}
        with self.__lock:
            try:
                proxy = None
                # proxy = {"http": "http://localhost:8888"}
                url = "http://%s:%d/settings/%s" % (self.__host, self.__port, self.__addon_id)
                payload = {
                    "addonId": self.__addon_id,
                    "addonName": self.__addon_name,
                    "version": addon_version,
                    "modifiedTime": modified_time,
                    "settings": settings
                }
                result = requests.request('POST', url, json=payload, proxies=proxy)
                new_settings = result.json()
            except requests.RequestException, ex:
                xbmc.log("Error sending settings to Sync service: %s" % (ex.message, ), xbmc.LOGERROR)

        for setting_id, setting_value in new_settings.iteritems():
            if settings.get(setting_id, None) == setting_value:
                continue
            xbmc.log("[CloudSync] updating value for '%s'" % (setting_id, ), xbmc.LOGINFO)
            xbmcaddon.Addon.setSetting(self, setting_id, setting_value)
        return
