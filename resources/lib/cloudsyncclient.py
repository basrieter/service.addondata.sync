import datetime
import json
import os

from kodi import settings
from logger import Logger
from sync.sync_base import SyncBase


class CloudSyncClient:
    SETTINGS = "settings"
    MODIFIED = "modifiedTime"

    def __init__(self, root_folder, sync_client, sync_group, local_cloud_settings_store,
                 selective_sync, abort_requested, dry_run=False):
        self.rootFolder = root_folder
        if not os.path.isdir(self.rootFolder):
            os.makedirs(self.rootFolder)

        self.syncGroup = sync_group
        self.cloudSettingStore = local_cloud_settings_store     # type: dict

        #  TODO: what to do with this?
        self.selectiveSync = selective_sync

        self.dryRun = dry_run
        if dry_run:
            Logger.warning("Dry-run exercising only!")

        # Abort callback
        self.__abortRequested = abort_requested

        # Actual sync client for a platform
        self.__destination = sync_client                        # type: SyncBase

        # mask the e-mail
        email = self.__destination.users_get_current_account().email
        user, domain = email.split("@")
        domain, ext = domain.rsplit(".")
        user = user if len(user) == 2 else "%s%s%s" % (user[0], "*" * (len(user) - 2), user[-1])
        domain = domain if len(domain) == 2 else "%s%s%s" % (domain[0], "*" * (len(domain) - 2), domain[-1])
        self.eMail = "%s@%s.%s" % (user, domain, ext)

        Logger.info("Created %s", self)
        return

    def close(self):
        Logger.info("Stopping %s.", self)
        del self.__destination

    def sync(self):
        Logger.info("Syncing %s", self)

        # What do we have remote?
        remote_root = self.__destination.convert_to_remote_path(self.rootFolder)
        remote_files = self.__destination.list_folder(remote_root, recurse=False)[0]

        # Read all the remote settings files to create a cloudSettingStore from those values even before an add-on
        # requests it.
        remote_add_ons = filter(lambda f: f.endswith(".settings.json"), remote_files.keys())
        remote_add_ons = map(lambda a: a.replace(".settings.json", ""), remote_add_ons)
        for add_on_id in set(self.cloudSettingStore.keys() + remote_add_ons):
            if not settings.is_add_on_installed(add_on_id):
                continue
            self.sync_add_on(add_on_id, remote_files)

        return datetime.datetime.utcnow()

    def sync_add_on(self, add_on_id, remote_files):
        Logger.info("[%s] Syncing Add-on '%s'", add_on_id, add_on_id)

        # retrieve the actual other add-on's settings
        local_settings = self.cloudSettingStore.get(add_on_id, None)
        settings_file = "%s.settings.json" % (add_on_id,)
        temp_path = os.path.join(self.rootFolder, settings_file)
        remote_path = self.__destination.convert_to_remote_path(temp_path)
        remote_settings = None

        try:
            # Case #1: Do we have local settings for this add-on? No, then we only need to load them from the cloud
            if local_settings is None:
                Logger.info("[%s] No local settings stored yet. Retrieving only for later use.", add_on_id)
                # the problem however is that we can do this, but we really don't have a local modified time
                # to compare it against.
                remote_settings = self.__retrieve_remote_settings(add_on_id, temp_path, remote_path)

                # Just double check in case the add-on registered in tne mean time.
                if add_on_id not in self.cloudSettingStore:
                    self.cloudSettingStore[add_on_id] = remote_settings
                    return

            # The current local settings
            Logger.debug("[%s] Local settings:\n%s", add_on_id, json.dumps(local_settings, indent=4))

            # Case #2: no remote settings.
            local_modified_dt = datetime.datetime.utcfromtimestamp(local_settings[CloudSyncClient.MODIFIED])
            if settings_file not in remote_files:
                # No remote data present, upload it.
                with file(temp_path, 'w+') as fp:
                    json.dump(local_settings, fp, indent=4)

                Logger.debug("[%s] Remote setting not found. Pushing to the cloud.", add_on_id)
                self.__destination.put_file(temp_path, remote_path, local_modified_dt)
                return

            # Retrieve the remote settings if we did not already did that
            if remote_settings is None:
                remote_settings = self.__retrieve_remote_settings(add_on_id, temp_path, remote_path)
            remote_modified_dt = datetime.datetime.utcfromtimestamp(remote_settings[CloudSyncClient.MODIFIED])

            # Case #3: settings are the same.
            if remote_settings[CloudSyncClient.SETTINGS] == local_settings[CloudSyncClient.SETTINGS]:
                Logger.info("[%s] Remote and Local settings are the same. Doing nothing", add_on_id)

            # Case #4: remote settings are newer.
            elif remote_settings[CloudSyncClient.MODIFIED] > local_settings[CloudSyncClient.MODIFIED]:
                Logger.info("[%s] Remote is newer (%s). Using them from now on.", add_on_id, remote_modified_dt)
                self.cloudSettingStore[add_on_id] = remote_settings

            # Case #5: local settings are newer
            elif remote_settings[CloudSyncClient.MODIFIED] < local_settings[CloudSyncClient.MODIFIED]:
                Logger.info("[%s] Local is newer (%s). Pushing to the cloud.", add_on_id, local_modified_dt)
                # write the new settings and upload
                with file(temp_path, 'w+') as fp:
                    json.dump(local_settings, fp, indent=4)
                self.__destination.put_file(temp_path, remote_path, local_modified_dt)

            # Unknown action?
            else:
                Logger.warning("[%s] Remote and local have same modifiedTime "
                               "but different content. I am clueless.", add_on_id)

        finally:
            if temp_path and os.path.isfile(temp_path):
                os.remove(temp_path)

    def __retrieve_remote_settings(self, add_on_id, temp_path, remote_path):
        self.__destination.get_file(temp_path, remote_path, ignore_dry_run=True)
        with file(temp_path) as fp:
            remote_settings = json.load(fp)
        Logger.debug("[%s] Found remote setting: %s", add_on_id, json.dumps(remote_settings, indent=4))
        return remote_settings

    def __str__(self):
        if self.eMail and self.dryRun:
            return "%s for %s and group '%s' (Dry-run)" % (self.__destination, self.eMail, self.syncGroup)
        elif self.eMail:
            return "%s for %s and group '%s'" % (self.__destination, self.eMail, self.syncGroup)
        elif self.dryRun:
            return "%s without e-mail (Dry-run) for group '%s'" % (self.__destination, self.syncGroup,)

        return "%s without e-mail for group '%s'" % (self.__destination, self.syncGroup,)
