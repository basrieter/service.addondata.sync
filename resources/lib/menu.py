import os
import requests

import xbmc
import xbmcaddon

from kodi import dialog
from kodi import settings
from kodi.locker import LockWithDialog
from logger import Logger
from apiserver import ApiApp
from sync import dropbox_client
from sync import dryrun_client


@LockWithDialog()
def menu_action(action, add_on_id=None):
    if add_on_id:
        add_on = xbmcaddon.Addon(add_on_id)
    else:
        add_on = xbmcaddon.Addon()

    if action == "settings":
        add_on.openSettings()
        return

    add_on_id = add_on.getAddonInfo('id')
    add_on_data = xbmc.translatePath("special://profile/addon_data/").decode('utf-8')
    this_add_on_data = os.path.join(add_on_data, add_on_id)
    add_on_name = add_on.getAddonInfo("name")

    # Retrieve the add-ons settings
    add_on_settings = settings.get_add_on_settings(add_on)

    # noinspection SpellCheckingInspection
    log_path = os.path.join(this_add_on_data, "add_on_data_sync_menu.log")
    Logger.create_logger(log_path, add_on_name, add_on_settings.log_level)

    if not add_on_settings.dropbox_api_key or not add_on_settings.sync_group:
        raise ValueError("Missing configuration elements. Please configure the add-on.")

    # client = None
    try:
        if action == "sync":
            url = "http://%s:%s/trigger/sync" % (ApiApp.HOST_NAME, ApiApp.PORT)
            Logger.info("Forcing sync on %s", url)
            result = requests.get(url)
            if result.ok:
                Logger.info("Response: %s", result.text)
            else:
                Logger.error("Response: %s", result.text)
            message = add_on.getLocalizedString(30022)
            dialog.show_notification(add_on_name, message)
            return

        dbx = dropbox_client.DropBoxSync(add_on_settings.dropbox_api_key, add_on_settings.sync_group, add_on_data)
        if add_on_settings.dry_run:
            dbx = dryrun_client.DryRunClient(dbx, add_on_settings.sync_group, add_on_data)

        # TODO replace with a API call to retrieve a list of known add-ons.
        if action == "list":
            add_ons = requests.get("http://%s:%s/list" % (ApiApp.HOST_NAME, ApiApp.PORT)).json()
            add_ons_to_sync = map(lambda a: "%s %s" % (a["addonName"], a["version"]), add_ons["addons"])
            message = add_on.getLocalizedString(30018)
            dialog.select(heading=message, options=add_ons_to_sync)

        elif action == "send_logs":
            file_count = 0
            for file_name in os.listdir(this_add_on_data):
                if not file_name.endswith(".log"):
                    continue
                log_file = os.path.join(this_add_on_data, file_name)
                remote_file = dbx.convert_to_remote_path(log_file)
                dbx.put_file(log_file, remote_file)
                file_count += 1

            message = add_on.getLocalizedString(30009) % (file_count, )
            dialog.show_notification(add_on_name, message)
        else:
            raise ValueError("Invalid action: %s" % (action, ))

    except:
        Logger.error("Error performing menu action: %s", menu_action, exc_info=True)
        raise
    finally:
        Logger.get_instance().close_log()


def __show_selective_sync_disabled_message(add_on, add_on_name):
    msg = add_on.getLocalizedString(30025)
    dialog.show_notification(add_on_name, msg, severity=2)
