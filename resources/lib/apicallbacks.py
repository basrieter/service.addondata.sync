import json

from logger import Logger

# The main store to keep our local settings and the trigger values
remote_sync_triggered = [False]
local_cloud_settings_store = {}


# Callbacks
def cloud_settings_received(rec_add_on_id, received_settings):
    Logger.debug("[%s] Received settings sync request: %s", rec_add_on_id, json.dumps(received_settings))
    Logger.trace(json.dumps(local_cloud_settings_store))

    local_settings = local_cloud_settings_store.get(rec_add_on_id)
    # Case #1: No local settings yet
    if local_settings is None:
        Logger.debug("[%s] No local settings yet, storing them.", rec_add_on_id)
        local_cloud_settings_store[rec_add_on_id] = received_settings
        return {}

    # Case #2: Identical settings
    if received_settings["settings"] == local_settings["settings"]:
        Logger.debug("[%s] Received and local settings are identical.", rec_add_on_id)
        return {}

    # Case #3: Local settings are newer
    if received_settings["modifiedTime"] < local_settings["modifiedTime"]:
        Logger.debug("[%s] Local settings are more recent. Returning them.", rec_add_on_id)
        return local_settings["settings"]

    # Case #4: Received settings are newer
    if received_settings["modifiedTime"] >= local_settings["modifiedTime"]:
        # store them as new local settings
        Logger.debug("[%s] Received settings are more recent. Updating local ones.", rec_add_on_id)
        local_cloud_settings_store[rec_add_on_id] = received_settings
        return {}

    Logger.warning("[%s] No action could be determined for received settings", rec_add_on_id)
    return {}


def list_cloud_sync_add_ons():
    result = {
        "addons": map(lambda a: dict(addonName=a["addonName"], version=a["version"]),
                      local_cloud_settings_store.values())
    }
    return result
