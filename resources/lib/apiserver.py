import json
import threading

from wsgiref.simple_server import make_server, WSGIServer, WSGIRequestHandler
from SocketServer import ThreadingMixIn
from bottle import Bottle
from bottle import request, response


# noinspection PyMethodMayBeStatic
class ApiApp(Bottle):
    HOST_NAME = "127.0.0.1"
    PORT = 5454

    def __init__(self, name, sync_callback, settings_callback, event_callback, list_callback, logger=None):
        super(ApiApp, self).__init__()
        self.name = name

        # routes
        self.route('/echo', callback=self.__echo, method='POST')
        self.route('/settings/<add_on_id>', callback=self.__settings, method='POST')
        self.route('/trigger/sync', callback=self.__force_sync)
        self.route('/list', callback=self.__list_add_ons, method='GET')

        # error handling
        self.error_handler[404] = self.__error
        self.error_handler[500] = self.__error

        # callbacks
        self.__onSettingsReceived = settings_callback
        self.__onSyncTriggered = sync_callback
        self.__onEventReceived = event_callback
        self.__onListSettings = list_callback

        # add logger and main event trigger hook
        self.__logger = logger
        self.add_hook("before_request", self.__trigger_event_received)

    def __index(self):
        return "Hello, my name is " + self.name

    def __force_sync(self):
        # noinspection PyArgumentList
        self.__onSyncTriggered()
        return self.__ok("Events Triggered", "Sync")

    def __echo(self):
        return request.json if request.json else request.body

    def __settings(self, add_on_id):
        received_data = request.json
        return self.__onSettingsReceived(add_on_id, received_data)

    def __list_add_ons(self):
        return self.__onListSettings()

    def __ok(self, message, trigger):
        result = {
            "message": message,
        }
        if trigger:
            result["triggered"] = [trigger, ]
        return result

    def __error(self, http_error):
        response.content_type = 'application/json'
        return json.dumps({"error": http_error.status_code})

    def __trigger_event_received(self):
        if self.__onEventReceived:
            self.__onEventReceived(request.urlparts.hostname, request.urlparts.port, request.path)


# https://www.electricmonk.nl/log/2016/02/15/multithreaded-dev-web-server-for-the-python-bottle-web-framework/
class ThreadingWSGIServer(ThreadingMixIn, WSGIServer):
    daemon_threads = True
    request_queue_size = 25


class QuietHandler(WSGIRequestHandler):
    def log_request(*args, **kw): pass


class MultiThreadedServer:
    def __init__(self, wsgi_app, logger=None, listen=ApiApp.HOST_NAME, port=ApiApp.PORT,
                 quiet=False, multi_thread=True):
        self.wsgi_app = wsgi_app
        self.listen = listen
        self.port = port
        self.quiet = quiet

        self.server = make_server(self.listen, self.port, self.wsgi_app,
                                  ThreadingWSGIServer if multi_thread else WSGIServer,
                                  QuietHandler if self.quiet else WSGIRequestHandler)
        self.__thread = None

        self.__logger = logger
        if self.__logger:
            self.__logger.info("Starting remote control on %s:%d", listen, port)

    def start(self):
        self.__thread = threading.Thread(target=self.server.serve_forever).start()

    def serve_forever(self):
        self.server.serve_forever()

    def stop(self):
        self.server.shutdown()


if __name__ == '__main__':
    def print_event(*args):
        print args

    app = ApiApp('SyncAPI', print_event, print_event, print_event, print_event)

    server = MultiThreadedServer(app, listen="127.0.0.1", port=5454)
    server.serve_forever()
