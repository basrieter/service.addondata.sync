# Kodi Add-on Setting Sync (for Dropbox)
This is a simple Kodi service add-on that sync (bi-directional) add-on settings. Currently only Dropbox is supported. It is still heavily in development.

## Backups! Yes, you need them
This add-on could *mess up* your Kodi's add-on settings. Before you continue to use this add-on, please, please, please, **backup** the `addon_data folders` on all machines and Kodi profiles you are going to run the sync with.  

## Prerequisites
You will need to have the following in order to make this all work:

1. Full backups of your _addon_data_ folders (yes, again a reminder!)
1. Kodi Krypton (or higher) installed on either Linux, Windows or Android.
1. A Dropbox account with some storage available.
1. A Dropbox Developer App API Key:
    * Currently this add-on does not (yet) provide OAuth authentication. So you will need to create your own app to make it work.
    * Head over to [Dropbox.com's App Console](https://www.dropbox.com/developers/apps).
    * Create a new app there with the Dropbox API.
    * Select the "_App Folder_" access option.
    * And give the app an appropriate name. 
    * Then go into the app's settings and select "_Generated access token_".
    * Configure that token in the Kodi Add-on settings for the Dropbox.
1. Think of a nice name for _sync group_

## How it works

### Sync Groups
Within the add-on settings for this add-on you specify a _Sync Group_. All _Kodi Add-on Setting Sync (for Dropbox)_ add-ons with the same _Sync Group_ configured will sync add-on settings with each other using Dropbox. You can create multiple _Sync Groups_ that group different Kodi installs or even Kodi logins into different _Sync Groups_. However, you can only sync each instance of the add-on to a single _Sync Group_.

### Supported add-ons only
Only the settings for add-ons that implement the option to Cloud Sync their settings will be synced. Enabling or disabling this syncing is done via that specific add-ons settings. Information for developers on how to enable your add-on can be found below.

## How is stuff synced?
Depending on the situation, add-on settings will either be uploaded or downloaded. The following table describes what will happen with the add-on settings in what scenario: 

Local \ Remote   | Added   | Existing | Non-Existing
-----------------|---------|----------|--------------
**Added**        | Sync most recent. | Should not happen but if then we sync the most recent. | Upload local settings.
**Existing**     | Should not happen but if then we sync the most recent. | Sync most recent, or if no changes then don't sync. | Upload local file. 
**Non-Existing** | Download remote settings. | Download remote settings. | No action (Duh!) 

Let's look into some examples:

* Settings exists both locally as remote. This will lead to _Sync most recent, or if no changes then don't sync_ and thus the add-on will check what the most recent set of settings are. If the local ones are newer, they will be uploaded. If the remote settings are newer, those settings will be downloaded. If they did not change, nothing will happen.
* What if I add a new add-on locally? The settings for that add-on are _Added_ locally an _Non-Existing_ remotely: so they will be uploaded. 
* In the previous case, other instances of Kodi will see the settings as _Added_ remotely and _Non-Existing_ locally and they will thus be downloaded.

## What does an add-on developer need to do in order for their settings to be synced?

Syncing settings is a two step approach and includes two components:

1. The _Kodi Add-on Setting Sync (for Dropbox)_ service, running at the background. This add-on does the actual syncing of the add-on settings with the cloud back-end. This is done at a certain and configurable interval. It also has a simple Web API that is used to communicate with the add-ons that want to sync their settings.   
1. The communication is done using a new  *cloud-aware* `Addon` class. The new class replaces the standard Kodi `xbmcaddon.Addon` class. The new class inherits from the `xbmcaddon.Addon` class and has the same method signatures, including the constructor signature. Whenever the class is instantiated, it will call the service's API and sends its settings and modified time stamp to the API. The API will return update settings if those are available. Those settings are then stored by the class as new add-on settings.

It is important to notice that it is the actual add-on that implements the new `Addon` class that does the storing of the new settings, not the service. An important consequence of this is that, although the service might have already retrieve updated settings, those settings will not be stored, until the first time the add-on runs, or calls the `Addon.setSetting` method.

As an add-on developer, implementing and enabling Cloud Sync is rather simple. There are actually three things that need to be done: 

### 1. Add a dependency to the addon.xml
Add the `service.addondata.sync` add-on as a dependent add-on to your own addon.xml. It can be added at the bottom of already existing dependencies.

```xml
<requires>
    <import addon="xbmc.metadata" version="1.0"/>
    <import addon="xbmc.python" version="2.14.0"/>
    <import addon="service.addondata.sync" version="0.2.0"/>
</requires>
```

### 2. Add the Cloud Sync settings to the settings.xml
In order for Cloud Sync to work, there are two settings that need to be included into your own add-on settings: `cloud_sync_enabled` and `cloud_sync_settings`:

Setting | Type | Visible to user | Purpose |
--------|------|---------|---------|
`cloud_sync_enabled`| bool | Yes or No* | Indicates whether the Cloud Sync feature is enabled for the specific add-on.  
`cloud_sync_settings` | string | No | Should contain a comma separated list of existing settings that should be synced. There should be no whitespaces between the settings: *"setting1,setting2"* and **not** *"setting1, setting2"*. 

**The visibility of the `cloud_sync_enabled` depends on the fact if you want the user to enable or disable the cloud syncing.*
 
 Here are two examples for the settings.xml file. The first one with hidden `cloud_sync_enabled` settings and the second one with visible `cloud_sync_enabled` settings: 
```xml
<settings>
    <category id="general" label="30009">
        <setting id="cloud_sync_enabled" default="true" />
        <setting id="cloud_sync_settings" default="stream_bitrate,hide_fanart" />
    </category>
</settings>        
```

Or make it possible for the user to toggle the Sync Feature by making the setting visible to the end-user:
```xml
<settings>
    <category id="general" label="30009">
        <setting id="cloud_sync_enabled" default="true" type="bool" label="00000" />
        <setting id="cloud_sync_settings" default="stream_bitrate,hide_fanart" />        
    </category>
</settings>        
```

If preferred it could be an opt-in feature by setting the default value to `false` instead of `true`.

### 3. Replace the `xbmcaddon.Addon` with the cloud-aware `Addon` class
Within your add-on Python code, start using the `cloudaddon.Addon` class, wherever were using the `xbmcaddon.Addon`. So instead of: 

```Python
import xbmcaddon

a = xbmcaddon.Addon()
a.setSetting("setting_id", "setting_value")
``` 
Use:
```Python
import cloudaddon

a = cloudaddon.Addon()
a.setSetting("setting_id", "setting_value")
``` 

If all is well, after starting your add-on the kodi.log should show lines like this:

```
01:15:05.670 T:2952    INFO: [CloudSync] Starting cloud-sync enabled Addon instance for your.addon.id with settings: setting_1, setting_2
```  

## Troubleshooting
If you run into any issues, make sure to enable DEBUG (or TRACE) logging for the first. This can be done via the add-ons settings of the _Kodi Add-on Setting Sync_ add-on. After another sync, you can upload your log files via the built-in option, again via the add-on settings.

With that log file you can create a new issue at the BitBucket [issue tracker](https://bitbucket.org/basrieter/service.addondata.sync/issues) and attach these log files with your issue. Other discussion can be one at the Kodi forum [thread](https://forum.kodi.tv/showthread.php?tid=319572).   
